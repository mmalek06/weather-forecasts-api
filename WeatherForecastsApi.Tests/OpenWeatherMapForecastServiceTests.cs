﻿using NSubstitute;
using System.Net;
using System.Threading.Tasks;
using WeatherForecastsApi.Services;
using WeatherForecastsApi.Services.Results;
using Xunit;

namespace WeatherForecastsApi.Tests {
    public class OpenWeatherMapForecastServiceTests {

        [Theory]
        [InlineData(null, "abc")]
        [InlineData("abc", null)]
        [InlineData(null, null)]
        public async Task WhenAnyArgumentIsNull_ThenStatusIsEqualToNotFound(string country, string city) {
            var substitute = Substitute.For<IWeatherForecastRequest>();
            var sut = new OpenWeatherMapForecastService(substitute);

            substitute.GetForecastFor(country, city).Returns(new WeatherApiResult { Response = null, Status = HttpStatusCode.NotFound });

            var result = await sut.GetForecastFor(country, city);

            Assert.True(result.Status == HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task WhenAllArgumentsAreGiven_ThenStatusIsEqualToOk() {
            var substitute = Substitute.For<IWeatherForecastRequest>();
            var sut = new OpenWeatherMapForecastService(substitute);
            var country = "a";
            var city = "b";

            substitute.GetForecastFor(country, city).Returns(new WeatherApiResult { Response = "{main: {temp: 0, humidity: 0}}", Status = HttpStatusCode.OK });

            var result = await sut.GetForecastFor(country, city);

            Assert.True(result.Status == HttpStatusCode.OK);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task WhenResponseIsEmpty_ThenStatusIsEqualToNotFound(string response) {
            var substitute = Substitute.For<IWeatherForecastRequest>();
            var sut = new OpenWeatherMapForecastService(substitute);
            var country = "a";
            var city = "b";

            substitute.GetForecastFor(country, city).Returns(new WeatherApiResult { Response = response, Status = HttpStatusCode.OK });

            var result = await sut.GetForecastFor(country, city);

            Assert.True(result.Status == HttpStatusCode.NotFound);
        }

        [Theory]
        [InlineData(290, 16.85)]
        [InlineData(5, -268.15)]
        public async Task TemperatureIsConvertedProperly(decimal kelvin, decimal celsius) {
            var substitute = Substitute.For<IWeatherForecastRequest>();
            var sut = new OpenWeatherMapForecastService(substitute);
            var country = "a";
            var city = "b";
            var a = $"{{main: {{temp: {kelvin}, humidity: 0}}";
            substitute.GetForecastFor(country, city).Returns(new WeatherApiResult { Response = $"{{main: {{temp: {kelvin}, humidity: 0}}}}", Status = HttpStatusCode.OK });

            var result = await sut.GetForecastFor(country, city);

            Assert.True(result.Data.Temperature.Value == celsius);
        }

    }
}
