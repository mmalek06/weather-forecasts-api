﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using WeatherForecastsApi.ViewModels;
using Xunit;

namespace WeatherForecastsApi.IntegrationTests {
    public class WeatherApiTests {

        [Fact]
        public async Task ResponseForWarsawPolandIsNotNull() {
            using (var client = new HttpClient()) {
                var result = await client.GetAsync(new Uri("http://localhost:59380/api/weather/poland/warsaw"));

                Assert.NotNull(result);                
            }
        }

        [Fact]
        public async Task ResponseForWarsawPolandContainsTemperatureInCelsius() {
            using (var client = new HttpClient()) {
                var result = await client.GetAsync(new Uri("http://localhost:59380/api/weather/poland/warsaw"));
                var responseJson = await result.Content.ReadAsStringAsync();
                var responseObj = JsonConvert.DeserializeObject<WeatherForecastViewModel>(responseJson);

                Assert.True(responseObj.Temperature.Format == "celsius");
            }
        }

        [Fact]
        public async Task WhenNoCountryIsGiven_ThenStatusCodeIs404() {
            using (var client = new HttpClient()) {
                var result = await client.GetAsync(new Uri("http://localhost:59380/api/weather//warsaw"));

                Assert.True(result.StatusCode == HttpStatusCode.NotFound);
            }
        }

    }
}
