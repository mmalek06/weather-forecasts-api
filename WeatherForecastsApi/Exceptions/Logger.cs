﻿using System.Diagnostics;
using System.Web.Http.ExceptionHandling;

namespace WeatherForecastsApi.Exceptions {
    // We could log the error using something like the ELK stack for example
    public class Logger : ExceptionLogger {

        public override void Log(ExceptionLoggerContext context) =>
            Trace.TraceError(context.ExceptionContext.Exception.ToString());

    }
}