﻿using System.Net;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Results;

namespace WeatherForecastsApi.Exceptions {
    public class Handler : ExceptionHandler {

        private const string LocalEnv = "local";

        private readonly HttpConfiguration _configuration;

        public Handler(HttpConfiguration config) {
            _configuration = config;
        }

        public override void Handle(ExceptionHandlerContext context) {
            var formatters = _configuration.Formatters;
            var negotiator = _configuration.Services.GetContentNegotiator();
            var message = AppSettings.Env == LocalEnv ? context.Exception.Message : "Something went wrong, please try again later.";
            var stackTrace = AppSettings.Env == LocalEnv ? context.Exception.StackTrace : "";

            context.Result = new NegotiatedContentResult<ErrorResponse>(
                HttpStatusCode.InternalServerError, 
                new ErrorResponse {
                    Message = message,
                    StackTrace = stackTrace
                }, 
                negotiator, 
                context.Request, 
                formatters);
        }

        internal class ErrorResponse {

            public string Message { get; set; }

            public string StackTrace { get; set; }

        }

    }
}