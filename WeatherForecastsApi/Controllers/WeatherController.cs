﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WeatherForecastsApi.Services;

namespace WeatherForecastsApi.Controllers {
    public class WeatherController : ApiController {

        private readonly IWeatherForecastService _weatherForecastService;

        public WeatherController(IWeatherForecastService weatherForecastService) {
            _weatherForecastService = weatherForecastService;
        }

        public async Task<HttpResponseMessage> Get(string country, string city) {
            if (country == null) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = $"{nameof(country)} cannot be null" });
            }
            if (city == null) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = $"{nameof(city)} cannot be null" });
            }

            var result = await _weatherForecastService.GetForecastFor(country, city);

            return result.Status == HttpStatusCode.OK ?
                Request.CreateResponse(HttpStatusCode.OK, result.Data) :
                Request.CreateResponse(result.Status);
        }

    }
}