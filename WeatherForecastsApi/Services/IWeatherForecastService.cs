﻿using System.Threading.Tasks;
using WeatherForecastsApi.Services.Results;

namespace WeatherForecastsApi.Services {
    public interface IWeatherForecastService {

        Task<WeatherForecastResult> GetForecastFor(string country, string city);

    }
}