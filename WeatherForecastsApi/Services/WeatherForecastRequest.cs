﻿using System.IO;
using System.Net;
using System.Threading.Tasks;
using WeatherForecastsApi.Services.Results;

namespace WeatherForecastsApi.Services {
    public class OpenWeatherForecastRequest : IWeatherForecastRequest {

        public async Task<WeatherApiResult> GetForecastFor(string country, string city) {
            var url = $"{AppSettings.OpenWeatherMapEndpoint}{country},{city}&appid={AppSettings.OpenWeatherMapApiKey}";
            var request = (HttpWebRequest)WebRequest.Create(url);

            try {
                using (var response = (HttpWebResponse)await request.GetResponseAsync()) {
                    using (var stream = response.GetResponseStream()) {
                        using (var reader = new StreamReader(stream)) {
                            return new WeatherApiResult {
                                Response = await reader.ReadToEndAsync(),
                                Status = HttpStatusCode.OK
                            };
                        }
                    }
                }
            } catch (WebException e) {
                using (var response = (HttpWebResponse)e.Response) {
                    return new WeatherApiResult {
                        Status = response.StatusCode
                    };
                }
            }
        }

    }
}