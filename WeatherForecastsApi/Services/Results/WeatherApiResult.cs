﻿using System.Net;

namespace WeatherForecastsApi.Services.Results {
    public class WeatherApiResult {

        public string Response { get; set; }

        public HttpStatusCode Status { get; set; }

    }
}