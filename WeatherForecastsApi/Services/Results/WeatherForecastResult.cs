﻿using System.Net;
using WeatherForecastsApi.ViewModels;

namespace WeatherForecastsApi.Services.Results {
    public class WeatherForecastResult {

        public HttpStatusCode Status { get; set; }

        public WeatherForecastViewModel Data { get; set; }

    }
}