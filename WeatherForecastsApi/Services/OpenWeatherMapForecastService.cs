﻿using Newtonsoft.Json;
using System.Net;
using System.Threading.Tasks;
using WeatherForecastsApi.Services.Results;
using WeatherForecastsApi.ViewModels;

namespace WeatherForecastsApi.Services {
    public class OpenWeatherMapForecastService : IWeatherForecastService {

        private const decimal KelvinDiff = 273.15m;

        private readonly IWeatherForecastRequest _forecastRequest;

        public OpenWeatherMapForecastService(IWeatherForecastRequest forecastRequest) {
            _forecastRequest = forecastRequest;
        }

        public async Task<WeatherForecastResult> GetForecastFor(string country, string city) {
            var response = await _forecastRequest.GetForecastFor(country, city);

            if (response.Status != HttpStatusCode.OK) {
                return new WeatherForecastResult { Status = response.Status };
            }
            if (string.IsNullOrEmpty(response.Response)) {
                return new WeatherForecastResult { Status = HttpStatusCode.NotFound };
            }

            var json = JsonConvert.DeserializeAnonymousType(response.Response, new {
                Main = new {
                    Temp = 0M,
                    Humidity = 0M
                }
            });

            return new WeatherForecastResult {
                Status = HttpStatusCode.OK,
                Data = new WeatherForecastViewModel {
                    Location = new LocationViewModel { City = city, Country = country },
                    Temperature = new TemperatureViewModel { Format = "celsius", Value = json.Main.Temp - KelvinDiff },
                    Humidity = json.Main.Humidity
                }
            };
        }

    }
}