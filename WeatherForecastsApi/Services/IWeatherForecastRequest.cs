﻿using System.Threading.Tasks;
using WeatherForecastsApi.Services.Results;

namespace WeatherForecastsApi.Services {
    public interface IWeatherForecastRequest {

        Task<WeatherApiResult> GetForecastFor(string country, string city);

    }
}