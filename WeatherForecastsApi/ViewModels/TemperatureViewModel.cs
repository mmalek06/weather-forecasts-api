﻿namespace WeatherForecastsApi.ViewModels {
    public class TemperatureViewModel {

        public string Format { get; set; }

        public decimal Value { get; set; }

    }
}