﻿namespace WeatherForecastsApi.ViewModels {
    public class LocationViewModel {

        public string City { get; set; }

        public string Country { get; set; }

    }
}