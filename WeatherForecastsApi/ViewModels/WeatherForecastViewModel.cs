﻿namespace WeatherForecastsApi.ViewModels {
    public class WeatherForecastViewModel {

        public LocationViewModel Location { get; set; }

        public TemperatureViewModel Temperature { get; set; }

        public decimal Humidity { get; set; }

    }
}