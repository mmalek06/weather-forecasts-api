﻿using System.Configuration;

namespace WeatherForecastsApi {
    public static class AppSettings {

        public static string Env => ConfigurationManager.AppSettings["env"];

        public static string OpenWeatherMapApiKey => ConfigurationManager.AppSettings["openWeatherMapApiKey"];

        public static string OpenWeatherMapEndpoint => ConfigurationManager.AppSettings["openWeatherMapEndpoint"];

    }
}