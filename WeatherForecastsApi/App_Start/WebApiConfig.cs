﻿using Autofac;
using Autofac.Integration.WebApi;
using Newtonsoft.Json.Serialization;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.ExceptionHandling;
using WeatherForecastsApi.Exceptions;
using WeatherForecastsApi.Services;

namespace WeatherForecastsApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config) {
            OutputConfig(config);
            DiConfig(config);
            CorsConfig(config);
            ExceptionsConfig(config);
            RoutingConfig(config);
        }

        private static void RoutingConfig(HttpConfiguration config) {
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "ForecastApi",
                routeTemplate: "api/{controller}/{country}/{city}"
            );
        }

        private static void ExceptionsConfig(HttpConfiguration config) {
            config.Services.Replace(typeof(IExceptionHandler), new Handler(config));
            config.Services.Replace(typeof(IExceptionLogger), new Logger());
        }

        private static void CorsConfig(HttpConfiguration config) {
            var cors = new EnableCorsAttribute("http://localhost:4200", "*", "GET,POST");

            config.EnableCors(cors);
        }

        private static void OutputConfig(HttpConfiguration config) {
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            config.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;

            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(
                config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml"));
        }

        private static void DiConfig(HttpConfiguration config) {
            var builder = new ContainerBuilder();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<OpenWeatherMapForecastService>()
                .As<IWeatherForecastService>()
                .WithParameter(new TypedParameter(typeof(IWeatherForecastRequest), new OpenWeatherForecastRequest()))
                .SingleInstance();

            var container = builder.Build();
            var resolver = new AutofacWebApiDependencyResolver(container);

            config.DependencyResolver = resolver;
        }

    }
}
